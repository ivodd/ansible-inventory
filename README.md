# DebConf Video Team Ansible Inventory

This Ansible inventory repo is where we keep our configuration for running the
[ansible roles](https://salsa.debian.org/debconf-video-team/ansible) in
production for various DebConfs.

If you are wanting to use our setup, fork and modify this repo to use with the
ansible roles we set up.
